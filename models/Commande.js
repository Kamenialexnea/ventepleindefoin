var mongoose = require('mongoose');

var Schema = mongoose.Schema

var CommandeSchema = new Schema({
    noCommande: {
        type: Number,
        required: true
    },
    dateCommande: {
        type: Date,
        required: true
    },
    noClient: {
        type: Schema.Types.ObjectId,
        ref: "Client",
        required: true
    }
})

var Commande = mongoose.model('Commande', CommandeSchema);

module.exports = Commande