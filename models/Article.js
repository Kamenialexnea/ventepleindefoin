var mongoose = require('mongoose');

var Schema = mongoose.Schema

var ArticleSchema = new Schema({
    noArticle: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    prixUnitaire: {
        type: Number,
        required: true
    },
    quantiteEnStock: {
        type: Number,
        required: true
    }
})

var Article = mongoose.model('Article', ArticleSchema);

module.exports = Article