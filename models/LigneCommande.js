var mongoose = require('mongoose');

var Schema = mongoose.Schema

var LigneCommandeSchema = new Schema({
    noCommande: {
        type: Schema.Types.ObjectId,
        ref: "Commande",
        required: true
    },
    noArticle: {
        type: Schema.Types.ObjectId,
        ref: "Article",
        required: true
    },
    quantite: {
        type: Number,
        required: true
    },
})

var LigneCommande = mongoose.model('LigneCommande', LigneCommandeSchema);

module.exports = LigneCommande