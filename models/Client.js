var mongoose = require('mongoose');

var Schema = mongoose.Schema

var ClientSchema = new Schema({
    noClient: {
        type: Number,
        required: true
    },
    nomClient: {
        type: String,
        required: true
    },
    noTelephone: {
        type: String,
        required: true
    }
})

var Client = mongoose.model('Client', ClientSchema);

module.exports = Client