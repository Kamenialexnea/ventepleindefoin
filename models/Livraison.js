var mongoose = require('mongoose');

var Schema = mongoose.Schema

var LivraisonSchema = new Schema({
    noLivraison: {
        type: Number,
        required: true
    },
    dateLivraison: {
        type: Date,
        required: true
    }
})

var Livraison = mongoose.model('Livraison', LivraisonSchema);

module.exports = Livraison