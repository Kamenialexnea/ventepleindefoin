var mongoose = require('mongoose');

var Schema = mongoose.Schema

var DetailLivraisonSchema = new Schema({
    noLivraison: {
        type: Number,
        required: true
    },
    noCommande: {
        type: Schema.Types.ObjectId,
        ref: "Commande",
        required: true
    },
    noArticle: {
        type: Schema.Types.ObjectId,
        ref: "Article",
        required: true
    },
    quantiteLivree: {
        type: Number,
        required: true
    },
})

var DetailLivraison = mongoose.model('DetailLivraison', DetailLivraisonSchema);

module.exports = DetailLivraison