﻿module.exports = {
    'mongoUrl': 'mongodb://localhost:27017/vente',
    // 'profileFoldier': 'public/images/profiles',
    // 'publicationFoldier': 'public/images/publications',
    'mongUrlTest': 'mongodb://localhost/testDatabaseVente',
    'secretKey': 'NancyApi1234',
    'deepModelCount': 5,
    'authExpiryAfter' : 12,
    'defaultPasswordSize' : 5,
    "verificationcodesize": 16,
}